﻿using System;

namespace FlexibleQuadTree
{
    public class Point
    {
        public Point() { }

        public Point( Double x, Double y )
        {
            _x = x;
            _y = y;
        }

        protected Double _x;

        public virtual Double X
        {
            get { return _x; }
        }

        protected Double _y;

        public virtual Double Y
        {
            get { return _y; }
        }
        
        public override String ToString()
        {
            return String.Format( "{0}/{1}", X, Y );
        }
    }
}