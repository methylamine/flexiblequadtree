﻿using FlexibleQuadTree.QueryStrategies;
using System;

namespace FlexibleQuadTree
{
    public class QuadTreeGps : QuadTree<CircleGps, PointGps>
    {
        public QuadTreeGps() : base( new CirclePointGpsQueryStrategy(), new Rectangle( 0, 0, 360, 180 ), 9, 60 ) {}

        public QuadTreeGps( Int32 maxDepth, Int32 maxContent ) : base( new CirclePointGpsQueryStrategy(), new Rectangle( 0, 0, 360, 180 ), maxDepth, maxContent ) {}
    }
}