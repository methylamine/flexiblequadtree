﻿using NUnit.Framework;
using System;

namespace FlexibleQuadTree.Tests
{
    [TestFixture]
    public class RectangleTests
    {
        [TestCase( -1, -1, false )]
        [TestCase( 0, 0, true )]
        [TestCase( 1, 1, true )]
        [TestCase( 2, 2, true )]
        [TestCase( 3, 3, true )]
        [TestCase( 4, 4, true )]
        [TestCase( 5, 5, true )]
        [TestCase( 6, 6, true )]
        [TestCase( 7, 7, true )]
        [TestCase( 8, 8, true )]
        [TestCase( 9, 9, true )]
        [TestCase( 10, 10, true )]
        [TestCase( 11, 11, false )]
        public void IntersectsWithSimpleTests( Double x, Double y, Boolean shouldIntersect )
        {
            var rectA = new Rectangle( 5, 5, 5, 5 );
            var rectB = new Rectangle( x, y, 5, 5 );

            var doesIntersect = rectA.IntersectsWith( rectB );
            Assert.AreEqual( shouldIntersect, doesIntersect );
        }

        [TestCase( 0, 0, false )]
        [TestCase( 1, 1, false )]
        [TestCase( 5, 5, true )]
        [TestCase( 6, 6, false )]
        [TestCase( 10, 10, false )]
        public void ContainsSimpleTests( Double x, Double y, Boolean shouldContain )
        {
            var rectA = new Rectangle( 5, 5, 5, 5 );
            var rectB = new Rectangle( x, y, 5, 5 );

            var contains = rectA.Contains( rectB );
            Assert.AreEqual( shouldContain, contains );
        }

        [Test]
        public void ContainsSmaller()
        {
            var rectA = new Rectangle( 5, 5, 5, 5 );
            var rectB = new Rectangle( 6, 6, 4, 4 );

            Assert.IsTrue( rectA.Contains( rectB ) );
        }

        [Test]
        public void DoesNotContainLarger()
        {
            var rectA = new Rectangle( 5, 5, 5, 5 );
            var rectB = new Rectangle( 4, 4, 6, 6 );

            Assert.IsFalse( rectA.Contains( rectB ) );
        }
    }
}