﻿using FlexibleQuadTree.QueryStrategies;
using NUnit.Framework;
using System;
using System.Diagnostics;
using System.Linq;

namespace FlexibleQuadTree.Tests
{
    [TestFixture]
    public class QuadTreeTests
    {
        private static readonly RectanglePointQueryStrategy s_queryStrategy = new RectanglePointQueryStrategy();
        
        //  Inserts various points at boundaries of 100x100 area, queries with 100x100 search rectangle expecting match each time.
        [TestCase( 0, 0 )]
        [TestCase( 0, 1 )]
        [TestCase( 1, 1 )]
        [TestCase( 1, 0 )]
        [TestCase( 2, 2 )]
        [TestCase( 100, 0 )]
        [TestCase( 0, 100 )]
        [TestCase( 100, 100 )]
        public void QueryMultiPointsLargeQueryAreaTests( Double x, Double y )
        {
            var tree = new QuadTree<Rectangle, Point>( s_queryStrategy, new Rectangle( 0, 0, 100, 100 ) );
            tree.Insert( new Point( x, y ) );

            var items = tree.Query( new Rectangle( 0, 0, 100, 100 ) );
            Assert.IsNotNull( items );
            Assert.AreEqual( 1, items.Count );
            Assert.AreEqual( x, items[ 0 ].X );
            Assert.AreEqual( y, items[ 0 ].Y );
        }

        //  Inserts one point at 50x50 in 100x100 rectangle, searches with 10x10 search rectangle at various positions to find matches
        [TestCase( 50, 50, true )]
        [TestCase( 0, 0, false )]
        [TestCase( 0, 90, false )]
        [TestCase( 90, 0, false )]
        [TestCase( 90, 90, false )]
        [TestCase( 45, 45, true )]
        [TestCase( 40, 40, true )]
        [TestCase( 49, 49, true )]
        [TestCase( 49, 41, true )]
        [TestCase( 41, 49, true )]
        public void QuerySinglePointMultiQueryAreaTests( Double x, Double y, Boolean expectHit )
        {
            var tree = new QuadTree<Rectangle, Point>( s_queryStrategy, new Rectangle( 0, 0, 100, 100 ) );
            tree.Insert( new Point( 50, 50 ) );

            var items = tree.Query( new Rectangle( x, y, 10, 10 ) );

            Assert.AreEqual( expectHit, items.Any() );

            if( !expectHit )
                return;

            Assert.IsNotNull( items );
            Assert.AreEqual( 1, items.Count );
            Assert.AreEqual( 50, items[ 0 ].X );
            Assert.AreEqual( 50, items[ 0 ].Y );

            Debug.WriteLine( tree.ToString() );
        }
    }
}
//testing BitBucket...