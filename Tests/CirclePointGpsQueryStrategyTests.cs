﻿using System;
using System.Diagnostics;
using System.Linq;
using FlexibleQuadTree.QueryStrategies;
using NUnit.Framework;

namespace FlexibleQuadTree.Tests
{
    [TestFixture]
    public class CirclePointGpsQueryStrategyTests
    {
        private static readonly CirclePointGpsQueryStrategy s_queryStrategy = new CirclePointGpsQueryStrategy();
        private static QuadTreeGps _gpsTree;

        [TestCase( 6, 9, false )]
        [TestCase( 6, 10, true )]
        [TestCase( 6, 15, true )]
        [TestCase( 6, 20, true )]
        [TestCase( 6, 21, false )]
        [TestCase( 24, 9, false )]
        [TestCase( 24, 10, true )]
        [TestCase( 24, 15, true )]
        [TestCase( 24, 20, true )]
        [TestCase( 24, 21, false )]
        [TestCase( 9, 6, false )]
        [TestCase( 10, 6, true )]
        [TestCase( 15, 6, true )]
        [TestCase( 20, 6, true )]
        [TestCase( 21, 6, false )]
        [TestCase( 9, 24, false )]
        [TestCase( 10, 24, true )]
        [TestCase( 15, 24, true )]
        [TestCase( 20, 24, true )]
        [TestCase( 21, 24, false )]
        [TestCase( 7.1716d, 7.1716d, true )]
        [TestCase( 7.170d, 7.170d, false )]
        [TestCase( 22.8284d, 7.1716d, true )]
        [TestCase( 22.829d, 7.1716d, false )]
        [TestCase( 7.1716d, 22.8284d, true )]
        [TestCase( 7.1716d, 22.829d, false )]
        [TestCase( 22.8284d, 22.8284d, true )]
        [TestCase( 22.829d, 22.8284d, false )]
        public void RectangleIntersectsQueryAreaTest( Double x, Double y, Boolean doesIntersect )
        {
            var rect = new Rectangle( 10, 10, 10, 10 );
            var circle = new CircleGps( y - 90, x - 180, 4 * 69.11d );

            Assert.AreEqual( doesIntersect, s_queryStrategy.RectangleIntersectsQueryArea( rect, circle ) );
        }

        [TestCase( 0, 0, false )]
        [TestCase( 10, 10, true )]
        [TestCase( 7.17d, 7.17d, false )]
        [TestCase( 7.1716d, 7.1716d, true )]
        [TestCase( 5, 10, false )]
        [TestCase( 6, 10, true )]
        [TestCase( 14, 10, true )]
        [TestCase( 15, 10, false )]
        public void ContentIntersectsQueryAreaTest( Double x, Double y, Boolean doesIntersect )
        {
            var point = new PointGps( y - 90, x - 180 );
            var circle = new CircleGps( 10 - 90, 10 - 180, 4 * 69.11d );

            Assert.AreEqual( doesIntersect, s_queryStrategy.ContentIntersectsQueryArea( point, circle ) );
        }

        [Test]
        public void RectangleContainsQueryAreaTest()
        {
            var circle = new CircleGps( 15 - 90, 15 - 180, 5 * 69.11d );

            var rect = new Rectangle( 10, 10, 10, 10 );
            Assert.IsTrue( s_queryStrategy.RectangleContainsQueryArea( rect, circle ) );

            rect = new Rectangle( 11, 11, 10, 10 );
            Assert.IsFalse( s_queryStrategy.RectangleContainsQueryArea( rect, circle ) );

            rect = new Rectangle( 9, 9, 10, 10 );
            Assert.IsFalse( s_queryStrategy.RectangleContainsQueryArea( rect, circle ) );

            rect = new Rectangle( 10, 10, 9, 10 );
            Assert.IsFalse( s_queryStrategy.RectangleContainsQueryArea( rect, circle ) );

            rect = new Rectangle( 10, 10, 10, 9 );
            Assert.IsFalse( s_queryStrategy.RectangleContainsQueryArea( rect, circle ) );

            circle = new CircleGps( 15 - 90, 15 - 180, 1 * 69.11d );
            rect = new Rectangle( 10, 10, 10, 10 );
            Assert.IsTrue( s_queryStrategy.RectangleContainsQueryArea( rect, circle ) );

            circle = new CircleGps( 15 - 90, 15 - 180, 6 * 69.11d );
            rect = new Rectangle( 10, 10, 10, 10 );
            Assert.IsFalse( s_queryStrategy.RectangleContainsQueryArea( rect, circle ) );
        }

        [Test]
        public void QueryAreaContainsRectangleTest()
        {
            var circle = new CircleGps( 10 - 90, 10 - 180, 5 * 69.11 );

            var rect = new Rectangle( 10, 10, 1, 1 );
            Assert.IsTrue( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );

            rect = new Rectangle( 1, 1, 1, 1 );
            Assert.False( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );

            rect = new Rectangle( 10, 1, 1, 1 );
            Assert.False( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );

            rect = new Rectangle( 1, 10, 1, 1 );
            Assert.False( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );

            rect = new Rectangle( 15, 15, 1, 1 );
            Assert.False( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );

            rect = new Rectangle( 8, 10, 1, 1 );
            Assert.IsTrue( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );

            rect = new Rectangle( 10, 8, 1, 1 );
            Assert.IsTrue( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );

            rect = new Rectangle( 8, 8, 1, 1 );
            Assert.IsTrue( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );

            rect = new Rectangle( 12, 12, 1, 1 );
            Assert.IsTrue( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );

            rect = new Rectangle( 7, 6, 6, 8 );
            Assert.IsTrue( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );

            circle = new CircleGps( 11 - 90, 10 - 180, 5 );
            rect = new Rectangle( 7, 6, 6, 8 );
            Assert.False( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );

            circle = new CircleGps( 10 - 90, 11 - 180, 5 );
            rect = new Rectangle( 7, 6, 6, 8 );
            Assert.False( s_queryStrategy.QueryAreaContainsRectangle( circle, rect ) );
        }

        /*
         * TODO: anonymize this
         */

        [Test]
        public void GulfCoastWaterwayPointsQueryTest()
        {
            var tree = new QuadTreeGps();

            #region Gulf West Intracoastal Canal GPS points

            tree.Insert( new PointGps( 28.72454, -95.86386 ) );
            tree.Insert( new PointGps( 28.72937, -95.84815 ) );
            tree.Insert( new PointGps( 28.73417, -95.83246 ) );
            tree.Insert( new PointGps( 28.73909, -95.81701 ) );
            tree.Insert( new PointGps( 28.74037, -95.72261 ) );
            tree.Insert( new PointGps( 28.74246, -95.73727 ) );
            tree.Insert( new PointGps( 28.74323, -95.80168 ) );
            tree.Insert( new PointGps( 28.74529, -95.70707 ) );
            tree.Insert( new PointGps( 28.74704, -95.78558 ) );
            tree.Insert( new PointGps( 28.74828, -95.76894 ) );
            tree.Insert( new PointGps( 28.7486, -95.75209 ) );
            tree.Insert( new PointGps( 28.75138, -95.69191 ) );
            tree.Insert( new PointGps( 28.75828, -95.67746 ) );
            tree.Insert( new PointGps( 28.76165, -95.64724 ) );
            tree.Insert( new PointGps( 28.76373, -95.66338 ) );
            tree.Insert( new PointGps( 28.7641, -95.63121 ) );
            tree.Insert( new PointGps( 28.77073, -95.61689 ) );
            tree.Insert( new PointGps( 28.77797, -95.60224 ) );
            tree.Insert( new PointGps( 28.78523, -95.58778 ) );
            tree.Insert( new PointGps( 28.79264, -95.57346 ) );
            tree.Insert( new PointGps( 28.79987, -95.55921 ) );
            tree.Insert( new PointGps( 28.80767, -95.5451 ) );
            tree.Insert( new PointGps( 28.81922, -95.53695 ) );
            tree.Insert( new PointGps( 28.83222, -95.53198 ) );
            tree.Insert( new PointGps( 28.84019, -95.51852 ) );
            tree.Insert( new PointGps( 28.84847, -95.50325 ) );
            tree.Insert( new PointGps( 28.85588, -95.48942 ) );
            tree.Insert( new PointGps( 28.86261, -95.47528 ) );
            tree.Insert( new PointGps( 28.86541, -95.45944 ) );
            tree.Insert( new PointGps( 28.86801, -95.45201 ) );
            tree.Insert( new PointGps( 28.8704, -95.44446 ) );
            tree.Insert( new PointGps( 28.87872, -95.43092 ) );
            tree.Insert( new PointGps( 28.88719, -95.41758 ) );
            tree.Insert( new PointGps( 28.89598, -95.38393 ) );
            tree.Insert( new PointGps( 28.89602, -95.40379 ) );
            tree.Insert( new PointGps( 28.89627, -95.38876 ) );
            tree.Insert( new PointGps( 28.8972645, -95.3808476 ) );
            tree.Insert( new PointGps( 28.8981301, -95.3795167 ) );
            tree.Insert( new PointGps( 28.8990317, -95.3782304 ) );
            tree.Insert( new PointGps( 28.90151, -95.37418 ) );
            tree.Insert( new PointGps( 28.90984, -95.35958 ) );
            tree.Insert( new PointGps( 28.91871, -95.34651 ) );
            tree.Insert( new PointGps( 28.9259, -95.33173 ) );
            tree.Insert( new PointGps( 28.9266608, -95.3304663 ) );
            tree.Insert( new PointGps( 28.9273467, -95.3290354 ) );
            tree.Insert( new PointGps( 28.9281048, -95.3275636 ) );
            tree.Insert( new PointGps( 28.9287366, -95.3261122 ) );
            tree.Insert( new PointGps( 28.9295274, -95.3246678 ) );
            tree.Insert( new PointGps( 28.9301574, -95.3232552 ) );
            tree.Insert( new PointGps( 28.9310062, -95.3218903 ) );
            tree.Insert( new PointGps( 28.9319619, -95.320624 ) );
            tree.Insert( new PointGps( 28.9330788, -95.319538 ) );
            tree.Insert( new PointGps( 28.93406, -95.31832 ) );
            tree.Insert( new PointGps( 28.9354649, -95.3174856 ) );
            tree.Insert( new PointGps( 28.9367661, -95.3165507 ) );
            tree.Insert( new PointGps( 28.937754, -95.315542 ) );

            #endregion

            var area = new CircleGps( 28.83222, -95.53198, 15 );

            var finds = tree.Query( area );

            Assert.IsTrue( finds.Count > 10 );

            Debug.WriteLine( "{0} results from query.", finds.Count );

            foreach( var find in finds )
                Debug.WriteLine( find );
        }

        [Test]
        public void ProgressivelyAddedPointsQuadTreeGps()
        {
            var tree = new QuadTreeGps();
            var search = new CircleGps( 28, -95, 1 );

            var results = tree.Query( search );
            Assert.AreEqual( 0, results.Count );

            tree.Insert( new PointGps( 28, -95 ) );
            results = tree.Query( search );
            Assert.AreEqual( 1, results.Count );

            tree.Insert( new PointGps( 28.001, -95.001 ) );
            results = tree.Query( search );
            Assert.AreEqual( 2, results.Count );
        }

        [Test]
        public void ExceedBothMaxDepthMaxContentFailSafeTest()
        {
            var tree = new QuadTreeGps();
            var lat = 45d;
            var lon = 45d;

            for( var i = 0; i < 1000; i++ )
            {
                lat += 0.0001d;
                lon += 0.0001d;
                var point = new PointGps( lat, lon );
                tree.Insert( point );
            }

            ShowNode( tree.Root );
        }

        [Test]
        public void ExceedBothMaxDepthMaxContentFailSafeTest2()
        {
            var tree = new QuadTreeGps();
            var lat = 45d;
            var lon = 45d;

            for( var i = 0; i < 1000; i++ )
            {
                var point = new PointGps( lat, lon );
                tree.Insert( point );
            }

            ShowNode( tree.Root );
        }

        private void ShowNode( Node<CircleGps, PointGps> node, Boolean showEmpties = false )
        {
            if( node.Contents.Any() || showEmpties )
                Debug.WriteLine( node );

            for( var i = 0; i < node._childNodes.Count; i++ )
                ShowNode( node._childNodes[ i ], showEmpties );
        }
    }
}