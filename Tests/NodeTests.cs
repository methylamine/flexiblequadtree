﻿using FlexibleQuadTree.QueryStrategies;
using NUnit.Framework;
using System;
using System.Diagnostics;

namespace FlexibleQuadTree.Tests
{
    [TestFixture]
    public class NodeTests
    {
        private static readonly RectanglePointQueryStrategy s_queryStrategy = new RectanglePointQueryStrategy();

        //  Inserts one point at known location, queries over that location expecting one match.
        [Test]
        public void InsertOneQueryOneTest()
        {
            var node = new Node<Rectangle, Point>( new Rectangle( 0, 0, 100, 100 ), 0 );
            node.Insert( new Point( 2, 3 ), s_queryStrategy );

            var items = node.Query( new Rectangle( 0, 0, 4, 4 ), s_queryStrategy );
            Assert.IsNotNull( items );
            Assert.AreEqual( 1, items.Count );
            Assert.AreEqual( 2, items[ 0 ].X );
            Assert.AreEqual( 3, items[ 0 ].Y );
        }

        //  Inserts 2K random points in 100x100 rectangle, queries w/ 100x100 search rectangle expecting all to match.
        [Test]
        public void InsertManyQueryAllTest()
        {
            var node = new Node<Rectangle, Point>( new Rectangle( 0, 0, 100, 100 ), 0, 8, 20 );

            var rnd = new Random();

            var NODE_COUNT = 2000;

            for( var i = 0; i < NODE_COUNT; i++ )
            {
                var x = rnd.NextDouble() * 100d;
                var y = rnd.NextDouble() * 100d;
                node.Insert( new Point( x, y ), s_queryStrategy );
            }

            var items = node.Query( new Rectangle( 0, 0, 100, 100 ), s_queryStrategy );
            Assert.IsNotNull( items );
            Assert.AreEqual( NODE_COUNT, items.Count );

            Debug.WriteLine( "{0} total nodes.", node.SubTreeContents.Count );
        }

        [Test]
        public void InsertQueryMultipleTests()
        {
            var node = new Node<Rectangle, Point>( new Rectangle( 0, 0, 10, 10 ), 0 );
            node.Insert( new Point( 1, 1 ), s_queryStrategy );
            node.Insert( new Point( 2, 2 ), s_queryStrategy );
            node.Insert( new Point( 3, 3 ), s_queryStrategy );
            node.Insert( new Point( 4, 4 ), s_queryStrategy );

            var items = node.Query( new Rectangle( 0, 0, 10, 10 ), s_queryStrategy );
            Assert.IsNotNull( items );
            Assert.AreEqual( 4, items.Count );

            PrintNode( node );
        }

        [Test]
        public void InsertQueryMultiplePerContentsTests()
        {
            var node = new Node<Rectangle, Point>( new Rectangle( 0, 0, 10, 10 ), 0, 10, 10 );

            node.Insert( new Point( 1, 1 ), s_queryStrategy );
            node.Insert( new Point( 1.001, 1.001 ), s_queryStrategy );
            node.Insert( new Point( 1.002, 1.002 ), s_queryStrategy );
            node.Insert( new Point( 1.003, 1.003 ), s_queryStrategy );
            node.Insert( new Point( 1.004, 1.004 ), s_queryStrategy );
            node.Insert( new Point( 1.005, 1.005 ), s_queryStrategy );
            node.Insert( new Point( 1.006, 1.006 ), s_queryStrategy );
            node.Insert( new Point( 1.007, 1.007 ), s_queryStrategy );

            var items = node.Query( new Rectangle( 0, 0, 10, 10 ), s_queryStrategy );
            Assert.IsNotNull( items );
            Assert.AreEqual( 8, items.Count );

            PrintNode( node );
        }

        private void PrintNode( Node<Rectangle, Point> node )
        {
            foreach( var child in node._childNodes )
                PrintNode( child );

            Debug.WriteLine( node );
        }
    }
}