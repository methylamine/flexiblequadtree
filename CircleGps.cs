﻿using FlexibleQuadTree.Interfaces;
using System;

namespace FlexibleQuadTree
{
    public class CircleGps : CircleBase
    {
        public CircleGps() {}

        public CircleGps( ILatitudeLongitude latLong, Double radiusMiles ) : this( latLong.Latitude, latLong.Longitude, radiusMiles ) {}

        public CircleGps( Double latitude, Double longitude, Double radiusMiles ) : this( latitude, longitude, radiusMiles, Guid.Empty ) {}

        public CircleGps( Double latitude, Double longitude, Double radiusMiles, Guid id )
        {
            Longitude = longitude;
            Latitude = latitude;
            Radius = radiusMiles;
            Id = id;
        }

        private const Double MILES_PER_DEGREE_AT_EQUATOR = 69.11d;

        public Guid Id { get; set; }

        public override Double X
        {
            get { return Longitude + 180d; }
        }

        public override Double Y
        {
            get { return Latitude + 90d; }
        }

        public Double Longitude { get; set; }

        public Double Latitude { get; set; }

        public Double RadiusMiles
        {
            get { return Radius; }
            set { Radius = value; }
        }

        /*
         * 
         * TODO: fix this, calcs depend on it and huge error nearer poles. Rethink.
         * 
         */
        public Double RadiusDegrees
        {
            get { return RadiusMiles / MILES_PER_DEGREE_AT_EQUATOR; }
        }
    }
}