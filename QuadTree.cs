﻿using FlexibleQuadTree.QueryStrategies;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlexibleQuadTree
{
    public class QuadTree<TQueryArea, TContent>
    {
        public QuadTree( IQueryStrategy<TQueryArea, TContent> queryStrategy, Rectangle rectangle ) : this( queryStrategy, rectangle, 4, 4 ) {}

        public QuadTree( IQueryStrategy<TQueryArea, TContent> queryStrategy, Rectangle rectangle, Int32 maxDepth, Int32 maxContents )
        {
            _queryStrategy = queryStrategy;
            _rectangle = rectangle;
            _root = new Node<TQueryArea, TContent>( _rectangle, 0, maxDepth, maxContents );
        }

        protected IQueryStrategy<TQueryArea, TContent> _queryStrategy;
        protected Node<TQueryArea, TContent> _root;
        protected Rectangle _rectangle;

        public virtual void Insert( TContent item )
        {
            _root.Insert( item, _queryStrategy );
        }

        public virtual List<TContent> Query( TQueryArea area )
        {
            return _root.Query( area, _queryStrategy );
        }

        public virtual Node<TQueryArea, TContent> Root
        {
            get { return _root; }
        }

        public virtual List<Node<TQueryArea, TContent>> Nodes
        {
            get
            {
                var nodesList = new List<Node<TQueryArea, TContent>>();
                GetNodesRecursive( Root, nodesList );
                return nodesList;
            }
        }

        private void GetNodesRecursive( Node<TQueryArea, TContent> node, List<Node<TQueryArea, TContent>> nodesList )
        {
            nodesList.Add( node );

            for( var i = 0; i < node._childNodes.Count; i++ )
                GetNodesRecursive( node._childNodes[ i ], nodesList );
        }

        public override String ToString()
        {
            var sb = new StringBuilder();
            sb.Append( "Rectangle:" ).AppendLine( _rectangle.ToString() );
            sb.Append( "Strategy: " ).AppendLine( _queryStrategy.ToString() );
            sb.Append( "Root: " ).AppendLine( "" );
            foreach( var node in _root._childNodes )
                sb.AppendLine( node.ToString() );

            return sb.ToString();
        }
    }
}