﻿using System;

namespace FlexibleQuadTree.QueryStrategies
{
    public interface IQueryStrategy<in TQueryArea, in TContent>
    {
        Boolean ContentIntersectsQueryArea( TContent content, TQueryArea queryArea );
        Boolean RectangleContainsContent( Rectangle rectangle, TContent content );
        Boolean RectangleContainsQueryArea( Rectangle rectangle, TQueryArea queryArea );
        Boolean RectangleIntersectsQueryArea( Rectangle rectangle, TQueryArea queryArea );
        Boolean QueryAreaContainsRectangle( TQueryArea queryArea, Rectangle rectangle );
    }
}