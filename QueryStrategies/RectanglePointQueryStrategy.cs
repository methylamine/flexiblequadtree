﻿
using System;

namespace FlexibleQuadTree.QueryStrategies
{
    public class RectanglePointQueryStrategy : IQueryStrategy<Rectangle, Point>
    {
        public Boolean ContentIntersectsQueryArea( Point content, Rectangle queryArea )
        {
            var result = Contains( queryArea, content );
            return result;
        }

        public Boolean RectangleContainsContent( Rectangle rectangle, Point content )
        {
            var result = Contains( rectangle, content );
            return result;
        }

        public Boolean RectangleContainsQueryArea( Rectangle rectangle, Rectangle queryArea )
        {
            var result = rectangle.Contains( queryArea );
            return result;
        }

        public Boolean RectangleIntersectsQueryArea( Rectangle rectangle, Rectangle queryArea )
        {
            var result = rectangle.IntersectsWith( queryArea );
            return result;
        }

        public Boolean QueryAreaContainsRectangle( Rectangle queryArea, Rectangle rectangle )
        {
            var result = queryArea.Contains( rectangle );
            return result;
        }

        private Boolean Contains( Rectangle rectangle, Point content )
        {
            var result = 
                rectangle.Left <= content.X && rectangle.Right >= content.X
                &&
                rectangle.Top <= content.Y && rectangle.Bottom >= content.Y;

            return result;
        }
    }
}
