﻿using System;

namespace FlexibleQuadTree.QueryStrategies
{
    public class CirclePointGpsQueryStrategy : IQueryStrategy<CircleGps, PointGps>
    {
        public Boolean ContentIntersectsQueryArea( PointGps content, CircleGps queryArea )
        {
            var absoluteDifferenceX = ( content.X - queryArea.X ).Abs();
            var absoluteDifferenceY = ( content.Y - queryArea.Y ).Abs();

            var hypotenuseSquared = absoluteDifferenceX.Squared() + absoluteDifferenceY.Squared();

            var result = hypotenuseSquared <= queryArea.RadiusDegrees.Squared();
            return result;
        }

        public Boolean RectangleContainsContent( Rectangle rectangle, PointGps content )
        {
            var result =
                rectangle.Left <= content.X && rectangle.Right >= content.X
                &&
                rectangle.Top <= content.Y && rectangle.Bottom >= content.Y;

            return result;
        }

        public Boolean RectangleContainsQueryArea( Rectangle rectangle, CircleGps queryArea )
        {
            var result =
                rectangle.Top <= queryArea.Y - queryArea.RadiusDegrees &&
                rectangle.Bottom >= queryArea.Y + queryArea.RadiusDegrees &&
                rectangle.Left <= queryArea.X - queryArea.RadiusDegrees &&
                rectangle.Right >= queryArea.X + queryArea.RadiusDegrees;

            return result;
        }

        public Boolean RectangleIntersectsQueryArea( Rectangle rectangle, CircleGps queryArea )
        {
            var rectCenterX = rectangle.X + rectangle.Width / 2d;
            var rectCenterY = rectangle.Y + rectangle.Height / 2d;

            var x = ( queryArea.X - rectCenterX ).Abs();
            var y = ( queryArea.Y - rectCenterY ).Abs();

            if( x > ( rectangle.Width / 2d + queryArea.RadiusDegrees ) )
                return false;

            if( y > ( rectangle.Height / 2d + queryArea.RadiusDegrees ) )
                return false;

            if( x <= ( rectangle.Width / 2d ) )
                return true;

            if( y <= ( rectangle.Height / 2d ) )
                return true;

            var cornerDistSquared = ( x - rectangle.Width / 2d ).Squared() + ( y - rectangle.Height / 2d ).Squared();

            return cornerDistSquared <= ( queryArea.RadiusDegrees ).Squared();
        }

        public Boolean QueryAreaContainsRectangle( CircleGps queryArea, Rectangle rectangle )
        {
            var circleLeft = queryArea.X - queryArea.RadiusDegrees;
            var circleRight = queryArea.X + queryArea.RadiusDegrees;
            var circleTop = queryArea.Y - queryArea.RadiusDegrees;
            var circleBottom = queryArea.Y + queryArea.RadiusDegrees;

            // get rid of easy cases first
            if( circleLeft > rectangle.Right )
                return false;

            if( circleRight < rectangle.Left )
                return false;

            if( circleTop > rectangle.Bottom )
                return false;

            if( circleBottom < rectangle.Top )
                return false;

            var radiusSquared = queryArea.RadiusDegrees.Squared();

            var topRightToCircleCenterSquared = ( rectangle.Top - queryArea.Y ).Squared() + ( rectangle.Right - queryArea.X ).Squared();
            if( radiusSquared < topRightToCircleCenterSquared )
                return false;

            var topLeftToCircleCenterSquared = ( rectangle.Top - queryArea.Y ).Squared() + ( rectangle.Left - queryArea.X ).Squared();
            if( radiusSquared < topLeftToCircleCenterSquared )
                return false;

            var bottomRightToCircleCenterSquared = ( rectangle.Bottom - queryArea.Y ).Squared() + ( rectangle.Right - queryArea.X ).Squared();
            if( radiusSquared < bottomRightToCircleCenterSquared )
                return false;

            var bottomLeftToCircleCenterSquared = ( rectangle.Bottom - queryArea.Y ).Squared() + ( rectangle.Left - queryArea.X ).Squared();
            if( radiusSquared < bottomLeftToCircleCenterSquared )
                return false;

            return true;
        }
    }
}