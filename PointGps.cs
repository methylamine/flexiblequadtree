﻿using System;

namespace FlexibleQuadTree
{
    public class PointGps : Point
    {
        public PointGps() {}

        public PointGps( Double latitude, Double longitude )
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public override Double X
        {
            get { return Longitude + 180d; }
        }

        public override Double Y
        {
            get { return Latitude + 90d; }
        }

        public Double Latitude { get; set; }

        public Double Longitude { get; set; }

        public override String ToString()
        {
            return String.Format( "{0}/{1}", Latitude, Longitude );
        }
    }
}