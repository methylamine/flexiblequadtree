﻿using System;

namespace FlexibleQuadTree
{
    public class CircleBase
    {
        public CircleBase() {}

        public CircleBase( Double x, Double y, Double radius )
        {
            _x = x;
            _y = y;
            Radius = radius;
        }

        protected Double _x;

        public virtual Double X
        {
            get { return _x; }
        }

        protected Double _y;

        public virtual Double Y
        {
            get { return _y; }
        }

        public virtual Double Radius { get; set; }
        
        public override String ToString()
        {
            return String.Format( "{0}/{1} R: {2}", X, Y, Radius );
        }
    }
}