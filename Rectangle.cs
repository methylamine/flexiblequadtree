﻿using System;

namespace FlexibleQuadTree
{
    public struct Rectangle 
    {
        public Rectangle( Double x, Double y, Double width, Double height )
        {
            _x = x;
            _y = y;
            _width = width;
            _height = height;
        }

        public static readonly Rectangle Empty = new Rectangle();
        private Double _x;
        private Double _y;
        private Double _width;
        private Double _height;

        public Double X
        {
            get { return _x; }
            set { _x = value; }
        }

        public Double Y
        {
            get { return _y; }

            set { _y = value; }
        }
        
        public Double Width
        {
            get { return _width; }

            set { _width = value; }
        }

        public Double Height
        {
            get { return _height; }

            set { _height = value; }
        }

        public Double Left
        {
            get { return X; }
        }

        public Double Top
        {
            get { return Y; }
        }

        public Double Right
        {
            get { return X + Width; }
        }

        public Double Bottom
        {
            get { return Y + Height; }
        }

        public Boolean IsEmpty
        {
            get
            {
                if( Width > 0.0 )
                    return Height <= 0.0;
                return true;
            }
        }
        
        public Boolean Contains( Rectangle rect )
        {
            var contains =
                this.Right >= rect.Right &&
                this.Left <= rect.Left &&
                this.Top <= rect.Top &&
                this.Bottom >= rect.Bottom;

            return contains;
        }
        
        public Boolean IntersectsWith( Rectangle rect )
        {
            var doesNotIntersect =
                this.Right < rect.Left ||
                this.Left > rect.Right ||
                this.Top > rect.Bottom ||
                this.Bottom < rect.Top;

            return !doesNotIntersect;
        }
        
        public void Offset( Double x, Double y )
        {
            X += x;
            Y += y;
        }

        public override String ToString()
        {
            return String.Format( "X = {0} Y = {1} Width = {2} Height = {3}", X, Y, Width, Height );
        }
    }
}