﻿using System;

namespace FlexibleQuadTree
{
    public class Circle : CircleBase
    {
        public Circle() {}
        public Circle( Double x, Double y, Double radius ) : base( x, y, radius ) {}
    }
}