﻿using System;

namespace FlexibleQuadTree
{
    public static class Extensions
    {
        public static Double RoundToThree( this Double number )
        {
            return Math.Round( number, 3 );
        }

        public static Double Abs( this Double number )
        {
            return Math.Abs( number );
        }

        public static Double Squared( this Double number )
        {
            return Math.Pow( number, 2 );
        }
    }
}