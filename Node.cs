﻿using FlexibleQuadTree.QueryStrategies;
using System;
using System.Collections.Generic;

namespace FlexibleQuadTree
{
    public class Node<TQueryArea, TContent>
    {
        public Node( Rectangle bounds ) : this( bounds, 0, 4, 4 ) {}

        public Node( Rectangle bounds, Int32 depth ) : this( bounds, depth, 4, 4 ) {}

        public Node( Rectangle bounds, Int32 depth, Int32 maxDepth, Int32 maxContents )
        {
            Bounds = bounds;
            Depth = depth;
            _maxDepth = maxDepth;
            _maxContents = maxContents;
        }

        private readonly Int32 _maxDepth;
        private readonly Int32 _maxContents;

        internal readonly List<Node<TQueryArea, TContent>> _childNodes = new List<Node<TQueryArea, TContent>>( 4 );

        public Rectangle Bounds { get; private set; }

        internal List<TContent> SubTreeContents
        {
            get
            {
                var results = new List<TContent>();

                for( var i = 0; i < _childNodes.Count; i++ )
                    results.AddRange( _childNodes[ i ].SubTreeContents );

                results.AddRange( Contents );

                return results;
            }
        }

        private List<TContent> _contents;

        public virtual List<TContent> Contents
        {
            get
            {
                if( _contents == null )
                    _contents = new List<TContent>();

                return _contents;
            }
        }

        public Boolean IsEmpty
        {
            get { return Bounds.IsEmpty || _childNodes.Count == 0; }
        }

        public Int32 Depth { get; set; }

        public virtual List<TContent> Query( TQueryArea queryArea, IQueryStrategy<TQueryArea, TContent> queryStrategy )
        {
            var results = new List<TContent>();

            for( var i = 0; i < Contents.Count; i++ )
            {
                var content = Contents[ i ];
                if( queryStrategy.ContentIntersectsQueryArea( content, queryArea ) )
                    results.Add( content );
            }

            for( var i = 0; i < _childNodes.Count; i++ )
            {
                var node = _childNodes[ i ];
                if( node.IsEmpty )
                    continue;

                if( queryStrategy.RectangleContainsQueryArea( node.Bounds, queryArea ) )
                {
                    results.AddRange( node.Query( queryArea, queryStrategy ) );
                    break;
                }

                if( queryStrategy.QueryAreaContainsRectangle( queryArea, node.Bounds ) )
                {
                    results.AddRange( node.SubTreeContents );
                    continue;
                }

                if( queryStrategy.RectangleIntersectsQueryArea( node.Bounds, queryArea ) )
                    results.AddRange( node.Query( queryArea, queryStrategy ) );
            }

            return results;
        }

        public virtual void Insert( TContent item, IQueryStrategy<TQueryArea, TContent> queryStrategy, Int32 depth = 0 )
        {
            if( !queryStrategy.RectangleContainsContent( Bounds, item ) )
                throw new Exception( String.Format( "count {0} '{1}' item cannot fit inside '{2}' Bounds.", Contents.Count, item, Bounds ) );

            if( _childNodes.Count == 0 )
                CreateChildNodes( depth );

            for( var i = 0; i < _childNodes.Count; i++ )
            {
                var childNode = _childNodes[ i ];

                if(
                    ( depth <= _maxDepth || Contents.Count >= _maxContents )
                    &&
                    queryStrategy.RectangleContainsContent( childNode.Bounds, item )
                    )
                {
                    childNode.Insert( item, queryStrategy, ++depth );
                    return;
                }
            }

            Contents.Add( item );
        }

        private void CreateChildNodes( Int32 depth )
        {
            var halfWidth = ( Bounds.Width / 2d );
            var halfHeight = ( Bounds.Height / 2d );

            _childNodes.Add( new Node<TQueryArea, TContent>( new Rectangle( Bounds.X, Bounds.Y, halfWidth, halfHeight ), depth, _maxDepth, _maxContents ) );
            _childNodes.Add( new Node<TQueryArea, TContent>( new Rectangle( Bounds.X, Bounds.Y + halfHeight, halfWidth, halfHeight ), depth, _maxDepth, _maxContents ) );
            _childNodes.Add( new Node<TQueryArea, TContent>( new Rectangle( Bounds.X + halfWidth, Bounds.Y, halfWidth, halfHeight ), depth, _maxDepth, _maxContents ) );
            _childNodes.Add( new Node<TQueryArea, TContent>( new Rectangle( Bounds.X + halfWidth, Bounds.Y + halfHeight, halfWidth, halfHeight ), depth, _maxDepth, _maxContents ) );
        }

        public override String ToString()
        {
            return String.Format( "L {0:#,##0.#00}, R {1:#,###.#00}, T {2:#,###.#00}, B {3:#,###.#00} depth {4} count {5}",
                                  Bounds.Left.RoundToThree(), Bounds.Right.RoundToThree(), Bounds.Top.RoundToThree(), Bounds.Bottom.RoundToThree(), Depth, Contents.Count );
        }
    }
}