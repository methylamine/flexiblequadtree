﻿using System;

namespace FlexibleQuadTree.Interfaces
{
    public interface ILatitudeLongitude
    {
        Double Latitude { get; set; }
        Double Longitude { get; set; }
    }
}